import 'dart:io';

import 'package:google_mobile_ads/google_mobile_ads.dart';

class AdMobService {
  static String? get bannerAdUnitId {
    if (Platform.isAndroid) {
      return 'ca-app-pub-3508798188034312/3243367471';
    }
    return null;
  }

  // static String? get interstitialAdUnitId {
  //   if (Platform.isAndroid) {
  //     return 'ca-app-pub-3940256099942544/8691691433';
  //   }
  //   return null;
  // }

  // static String? get rewardedAdUnitId {
  //   if (Platform.isAndroid) {
  //     return 'ca-app-pub-3940256099942544/9214589741';
  //   }
  //   return null;
  // }

  static final BannerAdListener bannerAdListener = BannerAdListener(
    onAdClicked: (ad) => print('Ad loaded'),
    onAdFailedToLoad: (ad, error) =>
        {ad.dispose(), print('Ad failed to load: $error')},
    onAdOpened: (ad) => print('Ad opened'),
    onAdClosed: (ad) => print('Ad closed'),
  );
}

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:new_app/pages/video_screen.dart';
import 'package:new_app/service/ad_mob.dart';
import 'package:new_app/service/api_service.dart';
import 'package:new_app/widgets/appBar.dart';
import 'package:new_app/widgets/appSearch.dart';
import 'package:new_app/models/list_video_model.dart';

class VideoPages extends StatefulWidget {
  @override
  _VideoPagesState createState() => _VideoPagesState();
}

class AdBannerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final BannerAd myBanner = BannerAd(
      // adUnitId: 'ca-app-pub-3508798188034312~2081881932',
      adUnitId: AdMobService.bannerAdUnitId!,
      size: AdSize.banner,
      request: const AdRequest(),
      listener: AdMobService.bannerAdListener,
    );

    myBanner.load();

    return Container(
      alignment: Alignment.center,
      child: AdWidget(ad: myBanner),
      width: myBanner.size.width.toDouble(),
      height: myBanner.size.height.toDouble(),
    );
  }
}

class _VideoPagesState extends State<VideoPages> {
  bool _isLoading = false;
  bool _isLoadingSeaech = false;
  bool _isLoadingMore = false;
  int _activeIndex = -1;
  String nextPage = "";
  String playList_id = "";
  String searchText = "";
  List<dynamic> keywords = [];
  List<dynamic> videos = [];
  ScrollController _scrollController = ScrollController();

  void initState() {
    super.initState();
    loadVideo("");
    _scrollController.addListener(_loadMoreListener);
  }

  void _loadMoreListener() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      if (nextPage != "") {
        setState(() {
          _isLoadingMore = true;
        });
        // Panggil API untuk memuat data tambahan
        loadMoreVideo();
        // print("Panggil");
      }
    }
  }

  void loadMoreVideo() async {
    var load =
        await APIService.instance.fetchVideo(nextPage, playList_id, searchText);
    var data = load['data'];
    setState(() {
      // keywords = data['playlist']['items'];
      videos.addAll(data['video']['items']);
      nextPage = data['video']['nextPageToken'];
      _isLoadingMore = false;
    });
  }

  void loadVideo(param) async {
    try {
      if (param == 'Search') {
        setState(() {
          _isLoadingSeaech = true;
        });
      } else {
        setState(() {
          _isLoading = true;
        });
      }
      var load = await APIService.instance
          .fetchVideo(nextPage, playList_id, searchText);
      if (load['success'] == true) {
        var data = load['data'];
        if (data['playlist'].length > 0) {
          setState(() {
            keywords = data['playlist']['items'];
            videos = data['video']['items'];
            nextPage = data['video']['nextPageToken'];
          });
        }
      }
      await Future.delayed(Duration(seconds: 2));
    } catch (error) {
      print('Error loading data: $error');
      if (param == 'Search') {
        setState(() {
          _isLoadingSeaech = false;
        });
      } else {
        setState(() {
          _isLoading = false;
        });
      }
    } finally {
      if (param == 'Search') {
        setState(() {
          _isLoadingSeaech = false;
        });
      } else {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  void updateFilter(String playlistId) {
    setState(() {
      playList_id = playlistId;
    });
    loadVideo('Search');
  }

  void updateSearchText(String text) {
    setState(() {
      searchText = text;
    });
    loadVideo('Search');
  }

  Widget renderLoadingLoadMore() {
    return _isLoadingMore
        ? Padding(
            padding: EdgeInsets.only(bottom: 80, top: 20),
            child: Column(
              children: [
                CircularProgressIndicator.adaptive(
                  valueColor: AlwaysStoppedAnimation<Color>(Color(0XFF1C53B4)),
                ),
              ],
            ),
          )
        : Container();
  }

  Widget renderLoading() {
    return Padding(
      padding: EdgeInsets.only(top: 130),
      child: Column(
        children: [
          CircularProgressIndicator.adaptive(
            valueColor: AlwaysStoppedAnimation<Color>(Color(0XFF1C53B4)),
          ),
        ],
      ),
    );
  }

  toDetail(BuildContext context, video) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => VideoScreen(id: video),
      ),
    );
  }

  format(time) {
    var data = DateTime.parse(time);
    DateFormat formatter = DateFormat('d MMM yyyy');
    String formatted = formatter.format(data);
    return formatted;
  }

  Widget renderContent() {
    if (_isLoadingSeaech) {
      return renderLoading();
    } else if (videos.length > 0) {
      return ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: videos.length,
          itemBuilder: (context, i) {
            var item = Video.fromJson(videos[i]);
            if (i % 5 == 0 && i > 1) {
              return Container(
                padding: EdgeInsets.only(top: 20, bottom: 20),
                child: AdBannerWidget(),
              );
            } else {
              return GestureDetector(
                onTap: () {
                  toDetail(context, videos[i]['id']);
                },
                child: Container(
                  margin: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      ClipRRect(
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(8.0),
                            topRight: Radius.circular(8.0)),
                        child: Stack(
                          children: [
                            Image.network(
                              '${item.thumbnails}',
                              fit: BoxFit.cover,
                              width: 360,
                              height: 180,
                            ),
                            Positioned.fill(
                              child: Align(
                                  alignment: Alignment.center,
                                  child: SvgPicture.asset(
                                      'assets/icons/lets-icons_video-fill.svg',
                                      height: 50,
                                      width: 50)),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 10, left: 10, top: 20),
                        margin: EdgeInsets.only(bottom: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                '${item.title} ',
                                style: const TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Color(0xff0A3944)),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(top: 20)),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  format('${item.publish}'),
                                  style: TextStyle(
                                      fontSize: 14, color: Color(0xff0A3944)),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                ),
                                Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(right: 8.0),
                                      child: Text('${item.viewCount}'),
                                    ),
                                    SvgPicture.asset(
                                        'assets/icons/iconamoon_eye-light.svg',
                                        height: 20,
                                        width: 20)
                                  ],
                                )
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              );
            }
          });
    } else {
      return Padding(
          padding: EdgeInsets.only(top: 200),
          child: Center(
            child: Text(
              ('errors').tr(),
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xffD6DEEA),
        appBar: CustomAppBar(),
        body: _isLoading
            ? Loading2()
            : ListView(controller: _scrollController, children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 20),
                  decoration: const BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                  ),
                  child: Column(
                    children: [
                      CustomAppSearch(onSubmit: updateSearchText),
                      const SizedBox(height: 20),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        padding: EdgeInsets.only(left: 20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            for (int i = 0; i < keywords.length; i++)
                              Row(
                                children: [
                                  TextButton(
                                    onPressed: () {
                                      if (_activeIndex == i) {
                                        setState(() {
                                          playList_id = "";
                                          _activeIndex = -1;
                                        });
                                      } else {
                                        updateFilter(keywords[i]['id']);
                                        setState(() {
                                          _activeIndex = i;
                                        });
                                      }
                                    },
                                    child: Text(
                                      ('${keywords[i]['title']}'),
                                      style: TextStyle(
                                        color: _activeIndex == i
                                            ? Colors.white
                                            : Color.fromRGBO(0, 0, 0, 1),
                                      ),
                                    ),
                                    style: TextButton.styleFrom(
                                      backgroundColor: _activeIndex == i
                                          ? Color(0xff1C53B4)
                                          : Color.fromARGB(255, 255, 255, 255),
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20, vertical: 0),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      side: BorderSide(
                                        color: Colors.black,
                                        width: 1.0,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(width: 10),
                                ],
                              ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                renderContent(),
                renderLoadingLoadMore()
              ]));
  }
}

class KeywordButton extends StatefulWidget {
  final String text;

  const KeywordButton({required this.text});

  @override
  _KeywordButtonState createState() => _KeywordButtonState();
}

class _KeywordButtonState extends State<KeywordButton> {
  bool _isActive = false;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        setState(() {
          _isActive = !_isActive;
        });
      },
      child: Text(
        widget.text,
        style: TextStyle(
          color: _isActive ? Colors.white : Colors.black,
        ),
      ),
      style: TextButton.styleFrom(
        backgroundColor: _isActive
            ? Color(0xff1C53B4)
            : const Color.fromARGB(255, 255, 255, 255),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        side: BorderSide(
          color: _isActive ? Colors.transparent : Colors.black,
          width: 1.0,
        ),
      ),
    );
  }
}

class Loading2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color(0xffD6DEEA),
        body: Center(
          child: CircularProgressIndicator(
            color: Color(0XFF1C53B4),
          ),
        ),
      ),
    );
  }
}

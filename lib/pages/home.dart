import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:new_app/config/apiconfig.dart';
import 'package:new_app/models/list_video_model.dart';
import 'package:new_app/models/playlist_model.dart';
import 'package:new_app/pages/article_detail.dart';
import 'package:new_app/pages/video.dart';
import 'package:new_app/pages/video_screen.dart';
import 'package:new_app/service/ad_mob.dart';
import 'package:new_app/service/api_service.dart';
import 'package:new_app/widgets/appBar.dart';
import 'package:new_app/widgets/appSearch.dart';
import 'package:new_app/widgets/loading.dart';

class HomePages extends StatefulWidget {
  final Function(int) onNavigateToVideo;
  const HomePages({Key? key, required this.onNavigateToVideo})
      : super(key: key);
  @override
  State<HomePages> createState() => _homePageState();
}

class AdBannerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final BannerAd myBanner = BannerAd(
      // adUnitId: 'ca-app-pub-3508798188034312~2081881932',
      adUnitId: AdMobService.bannerAdUnitId!,
      size: AdSize.banner,
      request:const AdRequest(),
      listener: AdMobService.bannerAdListener,
    );

    myBanner.load();

    return Container(
      alignment: Alignment.center,
      child: AdWidget(ad: myBanner),
      width: myBanner.size.width.toDouble(),
      height: myBanner.size.height.toDouble(),
    );
  }
}
class _homePageState extends State<HomePages> {
  void toVideo() {
    widget.onNavigateToVideo(2);
    // Anda bisa menambahkan logika lain yang ingin Anda jalankan di sini
  }

  void toArticle() {
    widget.onNavigateToVideo(1);
  }

  bool _isLoading = false;
  bool _isLoadingSeaech = false;
  String searchText = '';
  String playlist_id = '';
  int _activeIndex = -1;
  int nextPage = 1;
  int maxPage = 3;
  bool _isLoadingMore = false;
  List<dynamic> keywords = [];
  List<dynamic> videos = [];
  List<dynamic> articles = [];
  ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
    _loadAll("");
    _scrollController.addListener(_loadMoreListener);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _loadMoreListener() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      if (maxPage != (nextPage - 1)) {
        setState(() {
          _isLoadingMore = true;
          nextPage = nextPage + 1;
        });
        // Panggil API untuk memuat data tambahan
        _fetchMoreData();
        // print("Panggil");
      }
    }
  }

  void _fetchMoreData() async {
    var load = await APIService.instance
        .fetchLoadAll(nextPage, searchText, playlist_id);
    var data = load['data'];

    setState(() {
      articles.addAll(data['article']['data']);
      _isLoadingMore = false;
    });
    // print(data['article']['data']);
  }

  _loadAll(param) async {
    try {
      if (param == 'Search') {
        setState(() {
          _isLoadingSeaech = true;
        });
      } else {
        setState(() {
          _isLoading = true;
        });
      }
      var load = await APIService.instance
          .fetchLoadAll(nextPage, searchText, playlist_id);
      if (load['success'] == true) {
        var data = load['data'];
        setState(() {
          articles = data['article']['data'];
          maxPage = data['article']['last_page'];
        });
        if (data['playlist'].length > 0) {
          setState(() {
            keywords = data['playlist']['items'];
            videos = data['video']['items'];
          });
        }
      }

      await Future.delayed(Duration(seconds: 2));
    } catch (error) {
      print('Error loading data: $error');
      if (param == 'Search') {
        setState(() {
          _isLoadingSeaech = false;
        });
      } else {
        setState(() {
          _isLoading = false;
        });
      }
    } finally {
      if (param == 'Search') {
        setState(() {
          _isLoadingSeaech = false;
        });
      } else {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  format(time) {
    var data = DateTime.parse(time);
    DateFormat formatter = DateFormat('d MMM yyyy');
    String formatted = formatter.format(data);
    return formatted;
  }

  void updateSearchText(String text) async {
    setState(() {
      searchText = text;
    });
    _loadAll('Search');
  }

  void updateFilter(String playlistId) {
    setState(() {
      playlist_id = playlistId;
    });
    _loadAll('Search');
  }

  toDetail(BuildContext context, video) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => VideoScreen(id: video),
      ),
    );
  }

  toDetailArticle(BuildContext context, int index) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ArticleDetail(articleId: index),
      ),
    );
  }

  Widget renderLoadingLoadMore() {
    return _isLoadingMore
        ? Padding(
            padding: EdgeInsets.only(bottom: 80, top: 20),
            child: Column(
              children: [
                CircularProgressIndicator.adaptive(
                  valueColor: AlwaysStoppedAnimation<Color>(Color(0XFF1C53B4)),
                ),
              ],
            ),
          )
        : Container();
  }

  Widget renderLoading() {
    return Padding(
      padding: EdgeInsets.only(top: 130),
      child: Column(
        children: [
          CircularProgressIndicator.adaptive(
            valueColor: AlwaysStoppedAnimation<Color>(Color(0XFF1C53B4)),
          ),
        ],
      ),
    );
  }

  Widget renderContent() {
    if (_isLoadingSeaech) {
      return renderLoading();
    } else if (videos.length > 0 && articles.length > 0) {
      return Column(
        children: [
          Container(
            padding: EdgeInsets.only(right: 10, left: 20),
            color: Color(0xffD6DEEA),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      ('video').tr(),
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Row(children: [
                      TextButton(
                        onPressed: () {
                          toVideo();
                        },
                        child: Text(
                          ('seeAll').tr(),
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Color(0XFF1C53B4),
                          ),
                        ),
                      ),
                      SvgPicture.asset(
                        'assets/icons/RightArrow.svg',
                        width: 14,
                        height: 14,
                      ),
                    ]),
                  ),
                ),
              ],
            ),
          ),
          Column(
            children: [
              Container(
                padding: EdgeInsets.only(left: 10),
                color: Color(0xffD6DEEA),
                child: SizedBox(
                  height: 240,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: videos.length,
                    itemBuilder: (context, index) {
                      var data = Video.fromJson(videos[index]);
                      return Container(
                          margin: const EdgeInsets.all(12),
                          width: 250,
                          child: GestureDetector(
                            onTap: () {
                              toDetail(context, videos[index]['id']);
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                    borderRadius: BorderRadius.circular(8.0),
                                    child: Stack(
                                      children: [
                                        Image.network(
                                          '${data.thumbnails}',
                                          fit: BoxFit.cover,
                                          width: 250,
                                          height: 150,
                                        ),
                                        Positioned.fill(
                                          child: Align(
                                              alignment: Alignment.center,
                                              child: SvgPicture.asset(
                                                  'assets/icons/lets-icons_video-fill.svg',
                                                  height: 50,
                                                  width: 50)),
                                        ),
                                      ],
                                    )),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 10, left: 4, right: 6),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '${data.title}',
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Color(0xff0A3944)),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ));
                    },
                  ),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(right: 10, left: 20),
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      ('articles').tr(),
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                Container(
                    child: Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: Row(
                    children: [
                      TextButton(
                        onPressed: () {
                          toArticle();
                        },
                        child: Text(
                          ('seeAll').tr(),
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Color(0XFF1C53B4),
                          ),
                        ),
                      ),
                      SvgPicture.asset(
                        'assets/icons/RightArrow.svg',
                        width: 14,
                        height: 14,
                      ),
                    ],
                  ),
                )),
              ],
            ),
          ),
          Container(
              color: Colors.white,
              padding: EdgeInsets.only(bottom: 20.0, left: 20, right: 20),
              child: ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: articles.length,
                itemBuilder: (context, index) {
                if(index % 5 == 0  && index > 1){
                   return Container(
                    padding: EdgeInsets.only(top: 20,bottom: 20),
                    child: AdBannerWidget(),
                  );
                }else{
                    return GestureDetector(
                      onTap: () {
                        toDetailArticle(context, articles[index]['id']);
                      },
                      child: Container(
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 120.0,
                              height: 120.0,
                              margin: EdgeInsets.only(right: 16.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8.0),
                                image: DecorationImage(
                                  image: NetworkImage(BASE_URL_IMG +
                                      '${articles[index]['featured_image']}'),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: 2.0),
                                    width: 200,
                                    child: Text(
                                      '${articles[index]['title'.tr()]}',
                                      style: TextStyle(
                                          fontSize: 14.0,
                                          color: Color(0xff0A3944)),
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 3,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 30.0),
                                    width: 200,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          format('${articles[index]['date']}'),
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Color(0xff99A2B1)),
                                        ),
                                        Text(
                                          '${articles[index]['category']['name']}',
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Color(0xff99A2B1)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ));
                }
                },
              )),
        ],
      );
    } else {
      return Padding(
          padding: EdgeInsets.only(top: 200),
          child: Center(
            child: Text(
              ('errors').tr(),
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    // var height = MediaQuery.of(context).size.height;
    // var width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color(0xFFffffff),
      appBar: CustomAppBar(),
      body: _isLoading
          ? Loading()
          : ListView(
              controller: _scrollController,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(bottom: 20),
                  decoration: const BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                  ),
                  child: Column(
                    children: [
                      CustomAppSearch(onSubmit: updateSearchText),
                      const SizedBox(height: 20),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        padding: EdgeInsets.only(left: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            for (int i = 0; i < keywords.length; i++)
                              Row(
                                children: [
                                  TextButton(
                                    onPressed: () {
                                      if (_activeIndex == i) {
                                        setState(() {
                                          playlist_id = '';
                                          _activeIndex = -1;
                                        });
                                      } else {
                                        updateFilter(keywords[i]['id']);
                                        setState(() {
                                          _activeIndex = i;
                                        });
                                      }
                                    },
                                    child: Text(
                                      ('${keywords[i]['title']}'),
                                      style: TextStyle(
                                        color: _activeIndex == i
                                            ? Colors.white
                                            : Color.fromRGBO(0, 0, 0, 1),
                                      ),
                                    ),
                                    style: TextButton.styleFrom(
                                      backgroundColor: _activeIndex == i
                                          ? Color(0xff1C53B4)
                                          : Color.fromARGB(255, 255, 255, 255),
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20, vertical: 0),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      side: BorderSide(
                                        color: Colors.black,
                                        width: 1.0,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(width: 10),
                                ],
                              ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                renderContent(),
                renderLoadingLoadMore(),
              ],
            ),
    );
  }

  @override
  State<StatefulWidget> createState() {
    throw UnimplementedError();
  }
}

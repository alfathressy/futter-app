import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:new_app/service/ad_mob.dart';
import 'package:new_app/service/api_service.dart';
import 'package:new_app/widgets/loading.dart';

class ArticleDetail extends StatefulWidget {
  const ArticleDetail({Key? key, required this.articleId}) : super(key: key);
  final int articleId;

  @override
  State<ArticleDetail> createState() => _ArticleDetail();
}

class AdBannerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final BannerAd myBanner = BannerAd(
      // adUnitId: 'ca-app-pub-3508798188034312~2081881932',
      adUnitId: AdMobService.bannerAdUnitId!,
      size: AdSize.banner,
      request: const AdRequest(),
      listener: AdMobService.bannerAdListener,
    );

    myBanner.load();

    return Container(
      alignment: Alignment.center,
      child: AdWidget(ad: myBanner),
      width: myBanner.size.width.toDouble(),
      height: myBanner.size.height.toDouble(),
    );
  }
}

class _ArticleDetail extends State<ArticleDetail> {
  bool _isLoading = false;
  String htmlContent = "";
  String title = "";
  void initState() {
    super.initState();
    getDetail();
  }

  getDetail() async {
    setState(() {
      _isLoading = true;
    });
    var load = await APIService.instance.fetchArticleDetail(widget.articleId);
    var data = load['data'];
    setState(() {
      htmlContent = data['content'];
      title = data['title'];
    });
    await Future.delayed(Duration(seconds: 2));
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            title: Text(('titleArticle').tr())),
        backgroundColor: Color(0xFFFFFFFF),
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 20, top: 10),
                    child: Text(
                      title,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff0A3944),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 14, right: 14),
                    child: Html(
                      data: htmlContent,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 20, bottom: 20),
                    child: AdBannerWidget(),
                  ),
                ],
              )));
  }
}

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:intl/intl.dart';
import 'package:new_app/config/apiconfig.dart';
import 'package:new_app/service/api_service.dart';
import 'package:new_app/pages/article_detail.dart';
import 'package:new_app/widgets/loading.dart';
import '../widgets/appBar.dart';
import '../widgets/appSearch.dart';
import 'package:new_app/service/ad_mob.dart';

class ArticlePages extends StatefulWidget {
  const ArticlePages({Key? key}) : super(key: key);
  @override
  State<ArticlePages> createState() => articlePageState();
}

class AdBannerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final BannerAd myBanner = BannerAd(
      // adUnitId: 'ca-app-pub-3508798188034312~2081881932',
      adUnitId: AdMobService.bannerAdUnitId!,
      size: AdSize.banner,
      request:const AdRequest(),
      listener: AdMobService.bannerAdListener,
    );

    myBanner.load();

    return Container(
      alignment: Alignment.center,
      child: AdWidget(ad: myBanner),
      width: myBanner.size.width.toDouble(),
      height: myBanner.size.height.toDouble(),
    );
  }
}

class articlePageState extends State<ArticlePages> {
  bool _isLoading = false;
  bool _isLoadingSeaech = false;
  bool _isLoadingMore = false;
  String searchText = '';
  int category_id = 1;
  int _activeIndex = -1;
  int nextPage = 1;
  int maxPage = 3;
  List<dynamic> category = [];
  List<dynamic> articles = [];
  ScrollController _scrollController = ScrollController();

  void initState() {
    super.initState();
    loadArticles('');
    _scrollController.addListener(_loadMoreListener);
  }

  void _loadMoreListener() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      if (maxPage != (nextPage - 1)) {
        setState(() {
          _isLoadingMore = true;
          nextPage = nextPage + 1;
        });
        // Panggil API untuk memuat data tambahan
        _fetchMoreData();
        // print("Panggil");
      }
    }
  }

  void _fetchMoreData() async {
    var load = await APIService.instance
        .fetchArticles(nextPage, searchText, category_id);
    var data = load['data'];
    setState(() {
      articles.addAll(data['article']['data']);
      _isLoadingMore = false;
    });
  }

  loadArticles(param) async {
    try {
      if (param == 'Search') {
        setState(() {
          _isLoadingSeaech = true;
        });
      } else {
        setState(() {
          _isLoading = true;
        });
      }
      var load = await APIService.instance
          .fetchArticles(nextPage, searchText, category_id);
      var data = load['data'];
      setState(() {
        category = data['category'];
        articles = data['article']['data'];
        maxPage = data['article']['last_page'];
      });
      await Future.delayed(Duration(seconds: 2));
    } catch (error) {
      // Handle error, for example by showing an error message to the user
      print('Error loading data: $error');

      // Optionally, update the state to stop the loading indicator
      if (param == 'Search') {
        setState(() {
          _isLoadingSeaech = false;
        });
      } else {
        setState(() {
          _isLoading = false;
        });
      }
    } finally {
      if (param == 'Search') {
        setState(() {
          _isLoadingSeaech = false;
        });
      } else {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  format(time) {
    var data = DateTime.parse(time);
    DateFormat formatter = DateFormat('d MMM yyyy');
    String formatted = formatter.format(data);
    return formatted;
  }

  toDetail(BuildContext context, int index) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ArticleDetail(articleId: index),
      ),
    );
  }

  void updateFilter(int playlistId) {
    setState(() {
      category_id = playlistId;
    });
    loadArticles('Search');
  }

  void updateSearchText(String text) {
    setState(() {
      searchText = text;
    });
    loadArticles('Search');
  }

  Widget renderLoadingLoadMore() {
    return _isLoadingMore
        ? Padding(
            padding: EdgeInsets.only(bottom: 80, top: 20),
            child: Column(
              children: [
                CircularProgressIndicator.adaptive(
                  valueColor: AlwaysStoppedAnimation<Color>(Color(0XFF1C53B4)),
                ),
              ],
            ),
          )
        : Container();
  }

  Widget renderLoading() {
    return Padding(
      padding: EdgeInsets.only(top: 130),
      child: Column(
        children: [
          CircularProgressIndicator.adaptive(
            valueColor: AlwaysStoppedAnimation<Color>(Color(0XFF1C53B4)),
          ),
        ],
      ),
    );
  }

  Widget renderContent() {
    if (_isLoadingSeaech) {
      return renderLoading();
    } else if (articles.length > 0) {
      return Container(
          color: Colors.white,
          padding: EdgeInsets.only(bottom: 20.0),
          child: ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: articles.length,
              itemBuilder: (context, i) {
                if (i % 5 == 0 && i > 1) {
                  return Container(
                    padding: EdgeInsets.only(top: 20,bottom: 20),
                    child: AdBannerWidget(),
                  );
                } else {
                  return GestureDetector(
                    onTap: () {
                      toDetail(context, articles[i]['id']);
                    },
                    child: Container(
                      padding: EdgeInsets.only(left: 20.0, top: 10, bottom: 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: 120.0,
                            height: 120.0,
                            margin: EdgeInsets.only(right: 16.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.0),
                              image: DecorationImage(
                                image: NetworkImage(BASE_URL_IMG +
                                    '${articles[i]['featured_image']}'),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 2.0),
                                  width: 200,
                                  child: Text(
                                    '${articles[i]['title'.tr()]}',
                                    style: TextStyle(
                                        fontSize: 14.0,
                                        color: Color(0xff0A3944)),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 3,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 30.0),
                                  width: 200,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        format('${articles[i]['date']}'),
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: Color(0xff99A2B1)),
                                      ),
                                      Text(
                                        '${articles[i]['category']['name']}',
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: Color(0xff99A2B1)),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }
              }));
    } else {
      return Padding(
          padding: EdgeInsets.only(top: 200),
          child: Center(
            child: Text(
              ('errors').tr(),
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(),
        body: _isLoading
            ? Loading()
            : ListView(
                controller: _scrollController,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 20),
                    decoration: const BoxDecoration(
                      color: Color(0xffffffff),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10),
                      ),
                    ),
                    child: Column(
                      children: [
                        CustomAppSearch(onSubmit: updateSearchText),
                        const SizedBox(height: 20),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          padding: EdgeInsets.only(left: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              for (int i = 0; i < category.length; i++)
                                Row(
                                  children: [
                                    TextButton(
                                      onPressed: () {
                                        if (_activeIndex == i) {
                                          setState(() {
                                            category_id = 1;
                                            _activeIndex = -1;
                                          });
                                        } else {
                                          updateFilter(category[i]['id']);
                                          setState(() {
                                            _activeIndex = i;
                                          });
                                        }
                                      },
                                      child: Text(
                                        ('${category[i]['name']}'),
                                        style: TextStyle(
                                          color: _activeIndex == i
                                              ? Colors.white
                                              : Color.fromRGBO(0, 0, 0, 1),
                                        ),
                                      ),
                                      style: TextButton.styleFrom(
                                        backgroundColor: _activeIndex == i
                                            ? Color(0xff1C53B4)
                                            : Color.fromARGB(
                                                255, 255, 255, 255),
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 0),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        side: BorderSide(
                                          color: Colors.black,
                                          width: 1.0,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 10),
                                  ],
                                ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  renderContent(),
                  renderLoadingLoadMore(),
                ],
              ));
  }
}

class KeywordButton extends StatefulWidget {
  final String text;

  const KeywordButton({required this.text});

  @override
  _KeywordButtonState createState() => _KeywordButtonState();
}

class _KeywordButtonState extends State<KeywordButton> {
  bool _isActive = false;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        setState(() {
          _isActive = !_isActive;
        });
      },
      child: Text(
        widget.text,
        style: TextStyle(
          color: _isActive ? Colors.white : Colors.black,
        ),
      ),
      style: TextButton.styleFrom(
        backgroundColor: _isActive
            ? Color(0xff1C53B4)
            : const Color.fromARGB(255, 255, 255, 255),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        side: BorderSide(
          color: _isActive ? Colors.transparent : Colors.black,
          width: 1.0,
        ),
      ),
    );
  }
}

// class AdBannerWidget extends StatefulWidget {
//   @override
//   _AdBannerWidgetState createState() => _AdBannerWidgetState();
// }

// class _AdBannerWidgetState extends State<AdBannerWidget> {
//   BannerAd? _bannerAd;

//   @override
//   void initState() {
//     super.initState();
//     _bannerAd = BannerAd(
//       adUnitId: 'ca-app-pub-3940256099942544/6300978111', // Unit iklan uji
//       size: AdSize.banner,
//       request: AdRequest(),
//       listener: BannerAdListener(
//         onAdLoaded: (_) {
//           setState(() {});
//         },
//         onAdFailedToLoad: (ad, error) {
//           ad.dispose();
//           print('Ad failed to load: $error');
//         },
//       ),
//     )..load();
//   }

//   @override
//   void dispose() {
//     _bannerAd?.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return _bannerAd == null
//         ? SizedBox.shrink()
//         : Container(
//             alignment: Alignment.center,
//             child: AdWidget(ad: _bannerAd!),
//             width: _bannerAd!.size.width.toDouble(),
//             height: _bannerAd!.size.height.toDouble(),
//           );
//   }
// }

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:new_app/service/api_service.dart';
import 'package:new_app/widgets/admob.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';
// import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoScreen extends StatefulWidget {
  final String id;

  const VideoScreen({required this.id});

  @override
  _VideoScreenState createState() => _VideoScreenState();
}

class _VideoScreenState extends State<VideoScreen> {
  late YoutubePlayerController _controller;
  List<dynamic> detail = [];
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _controller = YoutubePlayerController.fromVideoId(
      videoId: widget.id,
      autoPlay: true,
      params: YoutubePlayerParams(
        showControls: true,
        mute: false,
        showFullscreenButton: false,
        loop: false,
      ),
    );
    getDetailVideo();
  }

  @override
  void dispose() {
    _controller.close();
    super.dispose();
  }

  void getDetailVideo() async {
    try {
      setState(() {
        _isLoading = true;
      });
      var load = await APIService.instance.fetchVideoDetail(widget.id);
      if (load['success'] == true) {
        var data = load['data'];
        setState(() {
          detail.add(data);
        });
      }
      await Future.delayed(Duration(seconds: 2));
    } catch (error) {
      print('Error loading data: $error');
      setState(() {
        _isLoading = false;
      });
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  format(time) {
    var data = DateTime.parse(time);
    DateFormat formatter = DateFormat('d MMM yyyy');
    String formatted = formatter.format(data);
    return formatted;
  }

  Widget renderLoading() {
    return Center(
      // Tambahkan CircularProgressIndicator di sini
      child: CircularProgressIndicator(
        color: Color(0XFF1C53B4),
      ),
    );
  }

  Widget content() {
    if (detail.length > 0) {
      return Column(
        children: [
          YoutubePlayer(
            controller: _controller,
            aspectRatio: 16 / 9,
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            padding: EdgeInsets.only(right: 20, left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '${detail[0]['title']}',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.left,
                ),
                Padding(padding: EdgeInsets.only(top: 20)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      format('${detail[0]['publish_date']}'),
                      style: TextStyle(fontSize: 14, color: Colors.white),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                    Row(children: [
                      Padding(
                        padding: EdgeInsets.only(right: 8.0),
                        child: Text('${detail[0]['statistics']['viewCount']}',
                            style: TextStyle(
                              color: Colors.white,
                            )),
                      ),
                      SvgPicture.asset('assets/icons/iconamoon_eye-light.svg',
                          height: 20, width: 20)
                    ])
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 20)),
                Text(
                  '${detail[0]['description']}',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          Container(
                padding: EdgeInsets.only(top: 20, bottom: 20),
                child: AdBannerWidget(),
              )
        ],
      );
    } else {
      return Center(
        child: Text(
          ('errors').tr(),
          style: TextStyle(
              color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff20202C),
      appBar: AppBar(
        backgroundColor: Color(0xff20202C),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(("video").tr(), style: TextStyle(color: Colors.white)),
      ),
      body: _isLoading ? renderLoading() : content(),
    );
  }
}

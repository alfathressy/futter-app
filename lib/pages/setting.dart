import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_app/main.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  runApp(EasyLocalization(
    supportedLocales: const [Locale('id'), Locale('en', 'US')],
    path: 'assets/translations',
    startLocale: const Locale('id'),
    child: const SettingPages(),
  ));
}


class SettingPages extends StatefulWidget {
  const SettingPages({Key? key}) : super(key: key);

  @override
  _SettingPagesState createState() => _SettingPagesState();
}

class _SettingPagesState extends State<SettingPages> {
  void updateParam(BuildContext context) {
    // Mengubah nilai param pada ParamProvider
    Provider.of<ParamProvider>(context, listen: false).updateParam();
    // Kembali ke halaman sebelumnya
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      home: MainPage(updateParam: updateParam),
    );
  }
}

class MainPage extends StatelessWidget {
  final Function(BuildContext) updateParam;

  const MainPage({Key? key, required this.updateParam}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('appBarTitle').tr(),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            // Mengubah bahasa
            Locale currentLocale = context.locale;
            Locale newLocale = currentLocale.languageCode == 'en'
                ? Locale('id')
                : Locale('en', 'US');
            context.setLocale(newLocale);
          },
          child: const Text('language').tr(),
        ),
      ),
    );
  }
}


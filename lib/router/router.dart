import 'package:flutter/material.dart';
import 'package:new_app/main.dart';
import 'package:new_app/pages/article_detail.dart';
import 'package:new_app/pages/setting.dart';
import 'package:new_app/pages/video.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => MyApp());
      case '/settings':
        return MaterialPageRoute(builder: (_) => SettingPages());
      case '/list_video':
        return MaterialPageRoute(builder: (_) => VideoPages());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('Page not found'),
                  ),
                ));
    }
  }
}

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:new_app/widgets/bottom_navigation.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  WidgetsFlutterBinding.ensureInitialized();
  MobileAds.instance.initialize();
  await EasyLocalization.ensureInitialized();

  runApp(EasyLocalization(
    child: MyApp(),
    supportedLocales: [
      Locale('id'),
      Locale('en', 'US'),
    ],
    path: 'assets/translations',
    startLocale: const Locale('id'),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _isLoading = true;
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 3), () {
      setState(() {
        _isLoading = false;
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      home: _isLoading
          ? LoadingScreen()
          : ChangeNotifierProvider(
              create: (context) => ParamProvider(),
              child: BottomNavigation(
                      value: _getLanguageCode(context.locale),
                    )
                  
            ),
    );
  }

  String _getLanguageCode(Locale locale) {
    return locale.toString() == 'en_US' ? 'EN' : 'ID';
  }
}

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              'assets/icons/Layer_x0020_1.svg',
              height: 50,
            ),
            Padding(padding: EdgeInsets.only(top: 30)),
            SizedBox(height: 20),
            CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Color(0xff1C53B4)),
            ),
          ],
        ),
      ),
    );
  }
}

class ParamProvider with ChangeNotifier {
  String _param = tr('parameter'); // Default value

  String get param => _param;
  void updateParam() {
    _param = tr('parameter');
    print('_param $_param');
    notifyListeners(); // Memberi tahu pendengar bahwa nilai telah berubah
  }
}

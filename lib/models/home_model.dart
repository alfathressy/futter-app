import 'package:new_app/models/article_model.dart';
import 'package:new_app/models/list_video_model.dart';
import 'package:new_app/models/playlist_model.dart';

class LoadAll {
  List<Playlist> playlists;
  List<Video> videos;
  List<ArticleItem> articles;
  LoadAll({
    required this.playlists,
    required this.videos,
    required this.articles,
  });

  factory LoadAll.fromMap(Map<String, dynamic> json) {
    List<dynamic> items = json['playlist']['items'];
    List<dynamic> itemsVideo = json['video']['items'];
    List<dynamic> itemsArticle = json['article']['data'];
    List<Playlist> playlists =
        items.map((item) => Playlist.fromJson(item)).toList();
    List<Video> videos = itemsVideo.map((item) => Video.fromJson(item)).toList();
    List<ArticleItem> articles =
        itemsArticle.map((item) => ArticleItem.fromJson(item)).toList();
        print(playlists);
    return LoadAll(playlists: playlists, videos: videos, articles: articles);
  }
}

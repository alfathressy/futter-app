class Playlist {
  final String? id;
  final String? title;

  Playlist({
     this.id,
     this.title,
  });

  factory Playlist.fromJson(Map<String, dynamic> json) {
    return Playlist(
      id: json['id'],
      title: json['title'],
    );
  }
}

class Video {
   String id = "";
   DateTime publish;
   String title ="";
   String description ="";
   String thumbnails ="";
   String viewCount ="";

  Video({
    required this.id,
    required this.publish,
    required this.title,
    required this.description,
    required this.thumbnails,
    required this.viewCount,
  });

  factory Video.fromJson(Map<String, dynamic> json) {
    return Video(
      id: json['id'],
      publish: DateTime.parse(json['publish']),
      title: json['title'],
      description: json['description'],
      thumbnails: json['thumbnails']['high']['url'],
      viewCount: json['statistic']['viewCount'],
    );
  }
}

class Thumbnail {
  final String url;
  final int width;
  final int height;

  Thumbnail({
    required this.url,
    required this.width,
    required this.height,
  });

  factory Thumbnail.fromJson(Map<String, dynamic> json) {
    return Thumbnail(
      url: json['url'],
      width: json['width'],
      height: json['height'],
    );
  }
}

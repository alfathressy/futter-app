class ArticleItem {
  int id;
  int categoryId;
  int userId;
  String date;
  String title;
  String content;
  String titleEn;
  String contentEn;
  String slug;
  String featuredImage;
  int readCount;
  int shareCount;
  int commentCount;
  int likeCount;

  ArticleItem({
    required this.id,
    required this.categoryId,
    required this.userId,
    required this.date,
    required this.title,
    required this.content,
    required this.titleEn,
    required this.contentEn,
    required this.slug,
    required this.featuredImage,
    required this.readCount,
    required this.shareCount,
    required this.commentCount,
    required this.likeCount,
  });

  factory ArticleItem.fromJson(Map<String, dynamic> json) {
    return ArticleItem(
      id: json['id'],
      categoryId: json['category_id'],
      userId: json['user_id'],
      date: json['date'],
      title: json['title'],
      content: json['content'],
      titleEn: json['title_en'],
      contentEn: json['content_en'],
      slug: json['slug'],
      featuredImage: json['featured_image'],
      readCount: json['read_count'],
      shareCount: json['share_count'],
      commentCount: json['comment_count'],
      likeCount: json['like_count'],
    );
  }
}

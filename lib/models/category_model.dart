class ArticleItem {
  int id;
  int cat_parent_id;
  String name;
  String slug;
  int is_parent;

  ArticleItem({
    required this.id,
    required this.cat_parent_id,
    required this.name,
    required this.slug,
    required this.is_parent,
  });

  factory ArticleItem.fromJson(Map<String, dynamic> json) {
    return ArticleItem(
      id: json['id'],
      cat_parent_id: json['cat_parent_id'],
      name: json['name'],
      slug: json['slug'],
      is_parent: json['is_parent'],
    );}
}

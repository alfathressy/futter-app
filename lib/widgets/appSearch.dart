// ignore: file_names
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomAppSearch extends StatelessWidget implements PreferredSizeWidget {
    final void Function(String) onSubmit;

  const CustomAppSearch({Key? key, required this.onSubmit}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 44,
      margin: const EdgeInsets.only(top: 10, left: 20, right: 20),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
            color: const Color(0xff1D1617).withOpacity(0.11),
            blurRadius: 40,
            spreadRadius: 0.0)
      ]),
      child: TextField(
         onSubmitted: onSubmit,
        decoration: InputDecoration(
            filled: true,
            fillColor: Color(0xffD6DEEA),
            contentPadding: const EdgeInsets.all(15),
            hintText: ('search').tr(),
            hintStyle: const TextStyle(color: Color(0xff99A2B2), fontSize: 14),
            suffixIcon: Padding(
                padding: const EdgeInsets.only(right: 20),
                child: SvgPicture.asset('assets/icons/IC_Search.svg',
                    height: 12.0, width: 12.0)),
                    
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide.none)),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

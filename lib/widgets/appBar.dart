// ignore: file_names
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_app/pages/setting.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  void toSettings(BuildContext context) {
    if (ModalRoute.of(context)?.settings?.name != '/settings') {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SettingPages()),
      );
    } else {
      debugPrint("gagal");
    }
  }

  void chnageLanguage(context) {}
  var flag = 'languageFlag'.tr();
  Widget build(BuildContext context) {
    return AppBar(
      toolbarHeight: 70,
      backgroundColor: const Color(0xffffffff),
      elevation: 0.0,
      centerTitle: false,
      title: Container(
          margin: const EdgeInsets.only(left: 10, top: 10),
          child: SvgPicture.asset(
            'assets/icons/Layer_x0020_1.svg',
            height: 30,
          )),
      actions: [
        Container(
          margin: EdgeInsets.only(top: 20),
          padding: EdgeInsets.only(right: 10),
          child: Row(
            children: [
              Text(
                ('languageFlag').tr(),
                style: TextStyle(
                    fontSize: 14), 
              ),
              IconButton(
                onPressed: () {
                  Locale currentLocale = context.locale;
                  Locale newLocale = currentLocale.languageCode == 'en'
                      ? Locale('id')
                      : Locale('en', 'US');
                  context.setLocale(newLocale);
                },
                icon: Container(
                    margin: const EdgeInsets.only(right: 4, left: 4),
                    width: 37,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 20,
                          offset: Offset(2, 0),
                        ),
                      ],
                    ),
                    child: flag == "Ind"
                        ? SvgPicture.asset(
                            'assets/icons/emojione_flag-for-indonesia.svg',
                            height: 30,
                            width: 30,
                          )
                        : SvgPicture.asset(
                            'assets/icons/emojione_flag-for-united-kingdom.svg',
                            height: 30,
                            width: 30,
                          )),
              )
            ],
          ),
        )
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text(
              'Drawer Header',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
          ListTile(
            title: Text('Settings'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SettingPages()),
              );
            },
          ),
          // Add more items if needed
        ],
      ),
    );
  }
}

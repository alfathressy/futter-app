import 'package:flutter/material.dart';

void main() {
  runApp(Loading());
}

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: CircularProgressIndicator(
            color: Color(0XFF1C53B4),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_app/main.dart';
import 'package:new_app/pages/home.dart';
import 'package:new_app/pages/video.dart';
import 'package:new_app/pages/article.dart';
import 'package:provider/provider.dart';

class BottomNavigation extends StatefulWidget {
  final String value;
  const BottomNavigation({Key? key, required this.value}) : super(key: key);

  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  int _currentIndex = 0;
  void setIndex(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    List screen = [
      HomePages(onNavigateToVideo: setIndex),
      ArticlePages(),
      VideoPages(),
    ];
    var bgColor;
    if (_currentIndex == 2) {
      bgColor = Color(0xffD6DEEA);
    } else {
      bgColor = Colors.white;
    }
    return Scaffold(
        backgroundColor: bgColor,
        body: screen[_currentIndex],
        bottomNavigationBar: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
              boxShadow: [
                BoxShadow(
                    color: Color.fromARGB(95, 117, 117, 117),
                    spreadRadius: 0,
                    blurRadius: 8),
              ],
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              ),
              child: BottomNavigationBar(
                onTap: (index) {
                  setState(() {
                    _currentIndex = index;
                  });
                },
                currentIndex: _currentIndex,
                items: [
                  BottomNavigationBarItem(
                    icon: SvgPicture.asset(
                      'assets/icons/bottomNavBar/Home.svg',
                      height: 24,
                      width: 24,
                    ),
                    activeIcon: SvgPicture.asset(
                      'assets/icons/bottomNavBar/HomeActive.svg',
                      height: 24,
                      width: 24,
                    ),
                    label: 'Home',
                  ),
                  BottomNavigationBarItem(
                    icon: SvgPicture.asset(
                      'assets/icons/bottomNavBar/Articles.svg',
                      height: 24,
                      width: 24,
                    ),
                    activeIcon: SvgPicture.asset(
                      'assets/icons/bottomNavBar/ArticlesActive.svg',
                      height: 24,
                      width: 24,
                    ),
                    label: 'Article',
                  ),
                  BottomNavigationBarItem(
                    icon: SvgPicture.asset(
                      'assets/icons/bottomNavBar/Video.svg',
                      height: 24,
                      width: 24,
                    ),
                    activeIcon: SvgPicture.asset(
                      'assets/icons/bottomNavBar/VideoActive.svg',
                      height: 24,
                      width: 24,
                    ),
                    label: 'Video',
                  ),
                ],
                showSelectedLabels: false,
                showUnselectedLabels: false,
              ),
            )));
  }
}
